/****************************************************************************
 **
 ** Copyright (c) 2015 ePapyrus, Inc. All rights reserved.
 **
 ** TableViewController.h
 ** UsingPlugPDFDocumentViewController
 **
 ** This file is part of PlugPDF for iOS project.
 **
 ****************************************************************************/

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@end
