var searchData=
[
  ['fields_3a',['fields:',['../interface_plug_p_d_f_document.html#a46600bde78a0b3e4ef78268e1de7d00e',1,'PlugPDFDocument']]],
  ['fields_3atitle_3a',['fields:title:',['../interface_plug_p_d_f_document.html#a66baa4fd1668156da88b28981d59b156',1,'PlugPDFDocument']]],
  ['fieldstate',['fieldState',['../interface_plug_p_d_f_base_field.html#aab5f344a4c05965980938357312026c6',1,'PlugPDFBaseField']]],
  ['fieldstatewithpage_3atitle_3a',['fieldStateWithPage:title:',['../interface_plug_p_d_f_document.html#ac150443b989726b6bf0d1a76ac2defc8',1,'PlugPDFDocument']]],
  ['fieldstatewithpage_3atitle_3aname_3a',['fieldStateWithPage:title:name:',['../interface_plug_p_d_f_document.html#a7b9c68a9488f5b81f212893b87128fcf',1,'PlugPDFDocument']]],
  ['fileattachmentannot',['FileAttachmentAnnot',['../interface_file_attachment_annot.html',1,'']]],
  ['fillcolor',['fillColor',['../interface_square_annot.html#a38dd1f2ee087f67eb0128e0071cd37c3',1,'SquareAnnot']]],
  ['fillfiledpermission',['fillFiledPermission',['../interface_plug_p_d_f_document.html#a6793f7a1da80ebc50b2cbec92ec7db0c',1,'PlugPDFDocument::fillFiledPermission()'],['../interface_plug_p_d_f_document_view_controller.html#a995db0545982924ec1e742d0377336b1',1,'PlugPDFDocumentViewController::fillFiledPermission()']]],
  ['findstring_3apage_3a',['findString:page:',['../interface_plug_p_d_f_document.html#a480fe1ebd7661fe277a4a1e14def07b0',1,'PlugPDFDocument']]],
  ['fittype',['fitType',['../interface_plug_p_d_f_document_view.html#a482a3a796bcad424a861fab2b26a51e3',1,'PlugPDFDocumentView::fitType()'],['../interface_plug_p_d_f_document_view_controller.html#a5019d752558d21e9b132379beddb17ff',1,'PlugPDFDocumentViewController::fitType()'],['../interface_plug_p_d_f_page_view.html#a1f60aed29d0a2a50b74b7eb70cf28fa0',1,'PlugPDFPageView::fitType()']]],
  ['flattenannotations',['flattenAnnotations',['../interface_plug_p_d_f_document.html#a0f03b121a747a066d23dff7038dc3f5e',1,'PlugPDFDocument::flattenAnnotations()'],['../interface_plug_p_d_f_document_view_controller.html#a399c9101a729bda44c9b594cda168b81',1,'PlugPDFDocumentViewController::flattenAnnotations()']]],
  ['flattenformfields_3ausecustomappearance_3a',['flattenFormFields:useCustomAppearance:',['../interface_plug_p_d_f_document_view_controller.html#af3b57ecf334ad8ac8918b29f7d5af8d8',1,'PlugPDFDocumentViewController']]],
  ['font',['font',['../interface_plug_p_d_f_text_field.html#acf551c5079a02ce5e6589e90dde12f64',1,'PlugPDFTextField']]],
  ['freetextannot',['FreeTextAnnot',['../interface_free_text_annot.html',1,'']]],
  ['',['',['../interface_plug_p_d_f_document.html#a3617cc1839ecfa74080027a5d4362edc',1,'PlugPDFDocument']]]
];
